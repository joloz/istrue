console.warn(
  "[isTrue] You appear to be using this package in your code.",
  "However, it was made to demonstrate how using a library instead of your own code can sometimes hurt your project.",
  "Please consider learning the IF statement."
);
exports.async = function (thing) {
  try {
    return new Promise(function (resolve, reject) {
      if (thing == true) {
        resolve(true);
      } else if (thing == false) {
        resolve(false);
      } else {
        console.warn(thing, "is neither true or false");
        reject();
      }
    });
  } catch (e) {
    console.error(e);
  }
};

exports.sync = function (thing) {
  try {
    if (thing == true) {
      return true;
    } else if (thing == false) {
      return false;
    } else {
      console.warn(thing, "is neither true or false");
      return undefined;
    }
  } catch (e) {
    console.error(e);
  }
};
