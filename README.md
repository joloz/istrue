# Checks if a value is true

⚠️ **DO NOT USE THIS IN PRODUCTION!** ⚠️
This is a package to demonstrate how using a library instead of your own code can sometimes hurt your project. If you actually considered using this package, please take a look at the [IF statement](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/if...else).

## Installation

Simply install `isTrue` in your project using NPM.

```bash
npm i value-is-true
```

and include it in your project by adding the following line at the top.

```nodejs
const isTrue = require("value-is-true");
```

## Usage

`isTrue` provides 2 ways of checking if a value is true.

### in sync

```nodejs
// Setting the input
var thingy = true;

// Checking if the input is true
var output = isTrue.sync(thingy);

// Printing the result to the console
console.log(output);
```

### async

```nodejs
// Setting the input
var thingy = true;

// Checking if the input is true
isTrue.async(thingy).then(function(result) => {
    //Output the result
    console.log(result);
})
```
