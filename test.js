var isTrue = require("./index");

console.log("Sync", isTrue.sync(false)); //false

isTrue.async(true).then((result) => {
  console.log("Async", result); //true
});
